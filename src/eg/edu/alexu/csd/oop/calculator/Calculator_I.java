package eg.edu.alexu.csd.oop.calculator;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
public class Calculator_I implements Calculator  {

	String s;String g;String r;String curr;
	double x,y;char op;
	Boolean error=false;
	
	String[] history = new String[6] ;
	static int currIndex=0; static int nextcounter=1; static int Prevcounter=1;
	
	 String[] historyRes = new String[6] ;

	 @Override
	public void input(String s)
	{		
		this.s=s;
		ArrayList<String> a = new ArrayList<>();
		ArrayList<String> opa = new ArrayList<>();
		ArrayList<Double> list = new ArrayList<>();
		String regex = "-?\\d+(\\.\\d+)?(\\*|\\+|-|\\/)-?\\d+(\\.\\d+)?";
		Pattern ptrn = Pattern.compile(regex);
		Matcher matcher = ptrn.matcher(s);
		if(matcher.matches())
		{
			curr=s;
			//matching Operators
			String regex1 = "[\\+\\-\\*\\/]";
			Pattern ptrn1 = Pattern.compile(regex1);
			Matcher matcher1 = ptrn1.matcher(s);
			while (matcher1.find()) {
			    a.add(matcher1.group());
			}
			if(a.size()>1)
			{
				String regex2 = "(?!(-?\\d+(\\.\\d+)?))[\\+\\-\\*\\/]";
				Pattern ptrn2 = Pattern.compile(regex2);
				Matcher matcher2 = ptrn2.matcher(s);
				while (matcher2.find()) {
				    opa.add(matcher2.group());
				}
				op=opa.get(0).charAt(0);
				//matching numbers
				String[] tokens = s.split("(?!(-?\\d+(\\.\\d+)?))[\\+\\-\\*\\/]");
				   for (String q : tokens) {
				        if (!(q.equals("") || q.equals("-")))
				        {
				        	list.add(Double.parseDouble(q));
				        }
				   }
				   x=list.get(0);
				   y =list.get(1);
			}
			else
			{
				op=a.get(0).charAt(0);
				//matching numbers
				String[] tokens2 = s.split("[^\\d\\.]");
				   for (String q : tokens2) {
				        if (!(q.equals("") || q.equals("-")))
				        {
				        	list.add(Double.parseDouble(q));
				        }
				   }
				   x=list.get(0);
				   y =list.get(1);
			}
		}
		else
		{
			error=true;
		}	
	}
	 
	 @Override
	public String getResult()
	{
		if(error)
		{
			error = false;
			return r=g="Error";
		}
		Double result= 0.0;
		if(op == '+')
		{
			result=  (double)x+(double)y;
		}
		else if( op == '-' )
		{
			result= (double)x-(double)y;
		}
		else if( op == '*' )
		{
			result=((double)x*(double)y);
		}
		else
		{
			result=((double)x/(double)y);
		}
		g = result.toString();
		r=g;
		return g ;
	}

	
	public void saveHist()
	{
		history[currIndex]=s;
		historyRes[currIndex]=r;
		if(currIndex+1 >5)
			currIndex=0;
		else
			currIndex++;
	}
	 @Override
	public String current ()
	{
		return curr;
	}
	 @Override
	public String prev()
	{
		int g = currIndex-Prevcounter;
		if(g<0)
		{
			g=g+6;
		}
		if( history[g] == null  || Prevcounter == 6  )
		{
			return "UNAVAILABLE";
		}
		Prevcounter++;
		return curr=history[g];	
	}
	 @Override
	public String next()
	{
		Prevcounter=Prevcounter-1;
		if(Prevcounter<=0)
		{
			Prevcounter++;
			return"UNAVAILABLE";
		}
		//System.out.println("next"+Prevcounter+" curr"+currIndex);
		int ke = currIndex-Prevcounter;
		if(ke<0)
		{
			ke=ke+6;
			return curr=history[ke];
		}
		else
		{
			return curr=history[ke];
		}
		
	}
	 @Override
	public void save()
	{
		File file = new File("save.txt");		
//		try {
//			System.out.println(file.getCanonicalPath());
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		} 
		FileWriter fw=null;
		try {
			fw = new FileWriter(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PrintWriter pw = new PrintWriter(fw);
		for(int i = 0 ; i < 6 ; i ++)
		{
			pw.println(history[i]);
		}
		pw.println(currIndex);
		pw.close();
	}
	 @Override
	public void load()
	{
	    File file = 
	    	      new File("E:\\JavaProjectsEclipse\\SimpleCalculator\\save.txt"); 
	    	    Scanner sc = null;
				try {
					sc = new Scanner(file);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
	    	  
//				int i=0;
//	    	    while (sc.hasNextLine()) 
//	    	    {
//	    	    	history[i]=sc.nextLine(); 
//	    	    	i++;
//	    	    }	    
	    	    for(int i = 0 ;i < 6 ; i++)
	    	    {
	    	    	history[i]=sc.nextLine(); 
	    	    }
	    	    currIndex=Integer.parseInt(sc.nextLine());
	}
}
