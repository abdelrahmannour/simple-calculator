package eg.edu.alexu.csd.oop.calculator;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class CalculatorGUI  {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	Calculator_I C = new Calculator_I();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalculatorGUI window = new CalculatorGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CalculatorGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 315, 499);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnNewButton_0 = new JButton("0");
		btnNewButton_0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				textField.setText(textField.getText() + btnNewButton_0.getText()); 
			}
		});
		btnNewButton_0.setBounds(78, 169, 71, 70);
		frame.getContentPane().add(btnNewButton_0);
		
		JButton btnNewButton_1 = new JButton("1");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				textField.setText(textField.getText() + btnNewButton_1.getText()); 
			}
		});
		btnNewButton_1.setBounds(10, 238, 71, 70);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("2");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + btnNewButton_2.getText()); 
			}
		});
		btnNewButton_2.setBounds(78, 238, 71, 70);
		frame.getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("3");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + btnNewButton_3.getText()); 
			}
		});
		btnNewButton_3.setBounds(147, 238, 71, 70);
		frame.getContentPane().add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("4");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + btnNewButton_4.getText()); 
			}
		});
		btnNewButton_4.setBounds(10, 309, 71, 70);
		frame.getContentPane().add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("5");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + btnNewButton_5.getText()); 
			}
		});
		btnNewButton_5.setBounds(78, 309, 71, 70);
		frame.getContentPane().add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("6");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + btnNewButton_6.getText()); 
			}
		});
		btnNewButton_6.setBounds(147, 309, 71, 70);
		frame.getContentPane().add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("7");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + btnNewButton_7.getText()); 
			}
		});
		btnNewButton_7.setBounds(10, 379, 71, 70);
		frame.getContentPane().add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("8");
		btnNewButton_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + btnNewButton_8.getText()); 
			}
		});
		btnNewButton_8.setBounds(78, 379, 71, 70);
		frame.getContentPane().add(btnNewButton_8);
		
		JButton btnNewButton_9 = new JButton("9");
		btnNewButton_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + btnNewButton_9.getText()); 
			}
		});
		btnNewButton_9.setBounds(147, 379, 71, 70);
		frame.getContentPane().add(btnNewButton_9);
		
		JButton btnNewButton_Prev = new JButton("<--");
		btnNewButton_Prev.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(C.prev()); 
				textField_1.setText("");
			}
		});
		btnNewButton_Prev.setBounds(10, 169, 71, 70);
		frame.getContentPane().add(btnNewButton_Prev);
		
		JButton btnNewButton_Next = new JButton("-->");
		btnNewButton_Next.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(C.next()); 
				 
			}
		});
		btnNewButton_Next.setBounds(147, 169, 71, 70);
		frame.getContentPane().add(btnNewButton_Next);
		
		textField = new JTextField();
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
//		textField.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				
//				String S = textField.getText();
//				C.input(S);		
//				textField_1.setText(C.getResult());
//			}
//		});
		textField.setBounds(10, 11, 201, 40);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
			}
		});
		textField_1.setColumns(10);
		textField_1.setBounds(10, 49, 201, 40);
		frame.getContentPane().add(textField_1);
		
		JButton btnNewButtonPlus = new JButton("+");
		btnNewButtonPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() +btnNewButtonPlus.getText()); 
			}
		});
		btnNewButtonPlus.setBounds(216, 238, 70, 70);
		frame.getContentPane().add(btnNewButtonPlus);
		
		JButton button_min = new JButton("-");
		button_min.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() +button_min.getText()); 
			}
		});
		button_min.setBounds(216, 169, 70, 70);
		frame.getContentPane().add(button_min);
		
		JButton button_multi = new JButton("*");
		button_multi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() +button_multi.getText());
			}
		});
		button_multi.setBounds(216, 309, 70, 70);
		frame.getContentPane().add(button_multi);
		
		JButton button_div = new JButton("/");
		button_div.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() +button_div.getText());
			}
		});
		button_div.setBounds(216, 379, 70, 70);
		frame.getContentPane().add(button_div);
		
		JButton btnC = new JButton("C");
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					textField.setText("");
					textField_1.setText("");
					C.Prevcounter=1;
					
				} catch (Exception e2) {
					// TODO: handle exception
				}

			}
		});
		btnC.setBounds(79, 119, 70, 49);
		frame.getContentPane().add(btnC);
		
		JButton button_equal = new JButton("=");
		button_equal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					String S = textField.getText();
					C.input(S);		
					textField_1.setText(C.getResult());
					C.saveHist();
					
//					textField_6.setText(C.history[4]);
//					textField_5.setText(C.history[3]);
//					textField_4.setText(C.history[2]);
//					textField_3.setText(C.history[1]);
//					textField_2.setText(C.history[0]);
					
				} catch (Exception e2) {
					// TODO: handle exception
				}

			}
		});
		button_equal.setBounds(221, 11, 70, 78);
		frame.getContentPane().add(button_equal);
		
		JButton btnLoad = new JButton("Load");
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				C.load();
			}
		});
		btnLoad.setBounds(11, 119, 70, 49);
		frame.getContentPane().add(btnLoad);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				C.save();
			}
		});
		btnSave.setBounds(147, 119, 70, 49);
		frame.getContentPane().add(btnSave);
		
		JButton btnNewButton = new JButton(".");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText()+".");
			}
		});
		btnNewButton.setBounds(216, 119, 70, 49);
		frame.getContentPane().add(btnNewButton);
	}
}
